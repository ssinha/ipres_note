
\section{Technical details}
\label{sec:tech}
This section shows the technicalities associated with the analysis. It explains how the data and simulation samples are obtained, what kind of selections are made on tracks used, and finally, how the reweighting of Monte Carlo samples are performed.
 
	\subsection{Ntuple generation}
	Dijet samples from IDTIDE are used to produce ntuples on the grid. Athena \texttt{AnalysisBase, 21.2.58} is utilised to perform the job submissions for ntuple production. The resultant ntuples so generated for data and Monte Carlo are stored in \texttt{DESY-HH\_LOCALGROUPDISK}.
	 
	\subsection{Event Selection}
	\label{subsection:selection}
	Analysis of different parameters pose different requirements on tracks. Hence, various track selections are used to analyse results efficiently. With this section, all the types of selections used in Sections \ref{sec:pv} and \ref{sec:beamspot} are explained.  

		\subsubsection{General selection}
		A selection common to all tracks is made in order to get rid of the inefficient tracks such that, only the events recorded during stable beam conditions, with a reconstructed primary vertex, satisfying detector requirements are taken into account.
		
		\textbf{Selection on jets:} Jets taken in this analysis are required to be within the Inner Detector pseudorapidity acceptance of |$\eta^{\textrm{jet}}$| < 2.5, satisfying cleaning criteria, originating from the primary hard-interaction vertex, and having $p_{T}^{\textrm{jet}}$> 20 GeV so as to reduce the effects due to pile-up interactions and underlying events. Jets with $p_{T}^{\textrm{jet}}$ < 60 jet GeV and |$\eta^{\textrm{jet}}$| < 2.4 are then required to be identified as originating from the primary vertex with a JVT output value greater than 0.59, to further reduce the contamination of jets arising from additional pile-up interactions.
		
		Additionally, for the analysis in Section \ref{sec:beamspot} for resolution with respect to beamspot (since a comparison is made with Monte Carlo), only the jets with $p_{T}>300$ GeV are taken. This is because the data and Monte Carlo show very different behaviour at low jet $p_T$.
		
		\textbf{Selection on tracks:} Tracks are required to have $p_T>500$ MeV and $|\eta|<2.5$. For a sensible analysis, tracks are divided into different categories of transverse momentum and pseudorapidity to ensure a reasonably constant and Gaussian resolution within a single subset. Hence, both the transverse, and the longitudinal impact parameter resolutions are measured for each track category binned in $p_T$ and $\eta$. Also, a cut on $\eta$ is chosen depending upon the stability of the IP resolution in that range. The $|\eta| < 0.8$ range corresponds to the barrel region, in which the resolution appears roughly constant.
		
		\subsubsection{Loose/Tight selection}
		\label{subsubsection:loosetight}
		Tracking CP supports two track selection working points for general use, Loose (representing the track quality requirements applied during reconstruction) and Tight Primary (an additional set of criteria to be applied to the reconstructed track collection, with the aim of keeping high efficiency for prompt/primary tracks while significantly reducing fakes).
		Table \ref{tab:loosetight} defines the Loose and tight criteria of selection of tracks. The definition of the corresponding variables are tabulated in Table \ref{tab:def_variables}. 
		
		\comment{
		\textbf{Loose:}		 
		 $p_T$ > 500 MeV,
		 $|\eta|$ < 2.5,
		 $N_{\textrm{Si}} \geq$ 7,
		 $N_{\textrm{mod}}^{\textrm{sh}}\leq$ 1,
		 $N_{\textrm{Si}}^{\textrm{hole}}\leq$ 2,
		 $N_{\textrm{pix}}^{\textrm{hole}}\leq$ 1. 
		 
		 \textbf{Tight Primary:}
		 $N_{\textrm{Si}}\geq9$ (if $|\eta|\leq 1.65$),
		 $N_{\textrm{Si}}\geq11$ (if $|\eta|\geq 1.65$),
		 $N_{\textrm{IBL}}+N_{\textrm{B-layer}}>0$ (if both IBL hit and B-layer hit are expected),
		 $N_{\textrm{Pix}}^{\textrm{hole}}=0$.
		}
		 \begin{table}[htbp]
		 	\caption{Selection cuts on tracks defined to be satisfying the "Loose" as well as the "Tight Primary" selection criteria}
		 	\label{tab:loosetight}
		 	\centering
		 	\renewcommand{\arraystretch}{1.4}
		 	\begin{tabular}{|l|l|@{}m{0pt}@{}}
		 		%\toprule
		 		\hline %&& \\
		 		\multicolumn{1}{|>{\centering\arraybackslash}m{23mm}|}{\textbf{Loose}} & \multicolumn{1}{>{\centering\arraybackslash}m{60mm}|}{\textbf{Tight Primary}} &\\
		 		\hline %&& \\
		 		%\midrule
		 		$p_T$ > 500 MeV& $N_{\textrm{Si}}\geq9$(if $|\eta|\leq 1.65$)\\
		 		$|\eta|$ < 2.5& $N_{\textrm{Si}}\geq11$(if $|\eta|\geq 1.65$)\\
		 		$N_{\textrm{Si}} \geq$ 7& $N_{\textrm{IBL}}+N_{\textrm{B-layer}}>0$ (if both IBL hit and\\
		 		$N_{\textrm{mod}}^{\textrm{sh}}\leq$ 1& B-layer hit are expected)\\
		 		$N_{\textrm{Si}}^{\textrm{hole}}\leq$ 2& $N_{\textrm{Pix}}^{\textrm{hole}}=0$\\
		 		$N_{\textrm{pix}}^{\textrm{hole}}\leq$ 1& \\
		 		\hline
		 		%\bottomrule
	 		\end{tabular}
 		\end{table}	
 	
		\begin{table}[htbp]
			\caption{Definition of variables used in Table \ref{tab:loosetight} to explain the loose and tight selection strategy}
			\label{tab:def_variables}
			\centering
			\renewcommand{\arraystretch}{1}
			\begin{tabular}{ll@{}m{0pt}@{}}
				\toprule
				Variable & \multicolumn{1}{c}{Definition} \\[3pt]
				\midrule
				Track $p_T$& Transverse momentum of the track\\[3pt]
				Track $|\eta|$& Absolute value of pseudorapidity of track\\[3pt]
				$N_{\textrm{IBL}}$& Number of IBL hits\\[3pt]
				$N_{\textrm{B-layer}}$& Number of b-layer hits\\[3pt]
				%$N_{\textrm{Pix}}$&Number of pixel hits (incl. IBL) + number of dead sensors the track crosses \\&in the pixel detector\\[3pt]
				%$N_{\textrm{SCT}}$&Number of SCT hits + number of SCT dead sensors\\[5pt]
				$N_{\textrm{Si}}$&Number of silicon hits (i.e. pixel + SCT) (including dead sensors)\\[3pt]
				%$N_{\textrm{Pix}}^{\textrm{sh}}$&Number of shared pixel hits: i.e. hits assigned to multiple tracks\\[3pt]
				%$N_{\textrm{SCT}}^{\textrm{sh}}$&Number of shared SCT hits: i.e. hits assigned to multiple tracks\\[3pt]
				%$N_{\textrm{Si}}^{\textrm{sh}}$&Number of shared silicon hits: i.e.hits assigned to multiple tracks\\[3pt]
				$N_{\textrm{Pix}}^{\textrm{hole}}$&Number of pixel holes, i.e. missing hits in the pixel detector\\[3pt]
				%$N_{\textrm{SCT}}^{\textrm{hole}}$&Number of SCT holes, i.e. missing hits in the pixel detector\\[3pt]
				$N_{\textrm{Si}}^{\textrm{hole}}$&Number of silicon holes, i.e. missing hits in the pixel detector and the SCT\\[3pt]
				$N_{\textrm{mod}}^{\textrm{sh}}$&Number of shared models = $N_{\textrm{Pix}}^{\textrm{sh}}$ + $N_{\textrm{SCT}}^{\textrm{sh}}$/2\\[3pt]
				\bottomrule
			\end{tabular}
		\end{table}		 

		\subsubsection{Track classification used in $b$-tagging}
		\label{subsubsection:btagclasses}
		The identification of $b$-quark jets in ATLAS is based on distinct methods described in three basic $b$-tagging algorithms: impact parameter-based algorithms, an inclusive secondary vertex reconstruction algorithm, and a decay chain multi-vertex reconstruction algorithm.  The output of these $b$-tagging algorithms are combined in a multivariate discriminant (MV2) which is the output of the default algorithm used by ATLAS, providing the best separation between the different jet flavours \cite{ATL-PHYS-PUB-2016-012}.
			
		Impact parameter based algorithms (IP2D and IP3D algorithms) use the signed impact parameter significance of tracks associated to a jet. IP2D algorithm uses only the transverse impact parameter as input, while IP3D uses both transverse and longitudinal components and their correlation. Probability density functions (PDF) obtained from reference histograms for the transverse and longitudinal impact parameter significances are derived from Monte Carlo simulation. They are separated into exclusive categories that depend on the hit pattern of a given track to increase the discriminating power and are used to calculate the ratios of the $b$- and light flavour jet probabilities. This computation is performed on a per-track basis. A quality criterion is defined from the knowledge of whether the hit is expected or not in the innermost (IBL) and in the next-to-innermost (b-layer) layers. The expectations for which hits should be present are derived from the information on the detector geometry and inactive module maps. Table \ref{tab:btagging_classes} shows the categories of tracks defined for the purpose.
			
		\begin{table}[htbp]
			\caption{Description of the track categories used by IP2D and IP3D $b-$tagging algorithms}
			\label{tab:btagging_classes}
			\centering
			\renewcommand{\arraystretch}{1}
			\begin{tabular}{ll@{}m{0pt}@{}}
				%\toprule
				\hline %&& \\
				\multicolumn{1}{>{\centering\arraybackslash}m{2mm}}{\textbf{\#}} & \multicolumn{1}{>{\centering\arraybackslash}m{105mm}}{\textbf{Category}} &\\
				\hline %&& \\
				%\midrule
				0 & No hits in first two layers; expected hit in IBL and b-layer\\
				1 & No hits in first two layers; expected hit in IBL and no expected hit in b-layer\\
				2 & No hits in first two layers; no expected hit in IBL and expected hit in b-layer\\
				3 & No hits in first two layers; no expected hit in IBL and b-layer\\
				4 & No hit in IBL; expected hit in IBL\\
				5 & No hit in IBL; no expected hit in IBL\\
				6 & No hit in b-layer; expected hit in b-layer\\
				7 & No hit in b-layer; no expected hit in b-layer\\
				8 & Shared hit in both IBL and b-layer\\
				9 & At least one shared pixel hits\\
				10 & Two or more shared SCT hits\\
				11 & Split hits in both IBL and b-layer\\
				12 & Split pixel hit\\
				13 & Good: a track not in any of the above categories\\
				\hline
				%\bottomrule
			\end{tabular}
		\end{table}	
			
			
		A hit is considered "split" if, during the ambiguity solver stage of the track reconstruction at the pattern recognition level \cite{ATL-PHYS-PUB-2015-018}, it was identified as being created by multiple charged particles (the decision is made by looking at its typical cluster shape \cite{ATL-PHYS-PUB-2015-006}\cite{collaboration_2014}). If a Pixel or SCT hit is associated to more than one track, and not already marked as split, it is regarded as "shared".

		Using this type of classification of tracks in this analysis helps to see how the impact parameter resolution varies for good tracks in comparison to the tracks of different qualities. 

		\subsubsection{Track classification on truth level}
		\label{subsubsection:truthselection}
		There are several qualities of tracks getting generated in Monte Carlo samples. In order to identify the tracks uniquely, each track is associated with an identifier number, called the "barcode". Depending on the value of this barcode, one can easily categorise the tracks required in the analysis. In general, with the barcodes, one can classify Monte Carlo tracks into the following:
		\begin{itemize}
			\item Barcode <10000: Represents primary, i.e., the prompt tracks
			\item 10000< Barcode <200000: Belongs to B/D-enriched tracks (secondary tracks)
			\item Barcode >200000: Corresponds to secondary tracks from \texttt{GEANT4}
		\end{itemize}
		
	\subsection{MC reweighting}
	The measurement of impact parameter resolution is a consequence of many effects, as mentioned in the introduction. These effects are complex to simulate and therefore, the performance of track IP reconstruction in MC may need an additional tuning \cite{BORISOV1996181} to reproduce the data precisely. Hence, a proper reweighting procedure is a prerequisite in order to get the simulated samples to match the data.
	
	The data and Monte Carlo used in this analysis show very different values at low jet $p_T$. Therefore, only the jets with $p_{T}>300$ GeV are taken. The reweighting on Monte Carlo is performed jet-wise. A two-dimensional histogram for number of tracks per jet, $N_{\textrm{trk}}^{\textrm{jet}}$, versus transverse momentum of jet, $p_{T}^\textrm{jet}$, is obtained for both, data and Monte Carlo samples. A ratio of the two histograms (corresponding to data and MC) is determined. This ratio gives the weight corresponding to each $N_{\textrm{trk}}^{\textrm{jet}}$-$p_{T}^\textrm{jet}$ bin. 
	
	\begin{figure}[htbp]
		\centering
		\subfloat[Before reweighting]{
			\includegraphics[width=0.46\textwidth]{../logos/figures/tech/class14jetpT_Nonreweighted.pdf}
			\label{fig:nonrew}
		}
		\centering
		\subfloat[After reweighting]{
			\includegraphics[width= 0.46\textwidth]{../logos/figures/tech/class14jetpT_reweighted.pdf}
			\label{fig:rew}
		}\newline \hspace*{-3mm}
		\centering
		\subfloat[Before reweighting]{
			\includegraphics[width=0.46\textwidth]{../logos/figures/tech/class14NTracksJet_Nonreweighted.pdf}
			\label{fig:nonrewN}
		}
		\centering
		\subfloat[After reweighting]{
			\includegraphics[width= 0.46\textwidth]{../logos/figures/tech/class14NTracksJet_reweighted.pdf}
			\label{fig:rewN}
		}
		\caption{\protect\subref{fig:nonrew} and \protect\subref{fig:rew} show the transverse momentum distribution for jets before and after reweighting, correspondingly. The distribution for number of tracks in jet is shown in \protect\subref{fig:nonrewN} before reweighting, and in \protect\subref{fig:rewN} after reweighting.
		}
		\label{fig:reweighting}
	\end{figure}
	
	Figure \ref{fig:reweighting} shows the plots for jet $p_T$ before and after reweighting in \ref{fig:nonrew} and \ref{fig:rew}, respectively. Similarly, the plots for number of tracks in jet before and after reweighing are shown in \ref{fig:nonrewN} and \ref{fig:rewN}. Evidently, the matching of data and MC becomes better after this reweighting procedure.
	