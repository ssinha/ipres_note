\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Technical details}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Ntuple generation}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Track Selection}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}General selection}{4}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Loose/Tight selection}{4}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Track classification based on $b$-tagging}{4}{subsubsection.2.2.3}
\contentsline {subsubsection}{\numberline {2.2.4}Track classification on truth level}{4}{subsubsection.2.2.4}
\contentsline {subsection}{\numberline {2.3}Deconvolution of primary vertex resolution from track impact parameter resolution}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Fitting function}{5}{subsection.2.4}
\contentsline {section}{\numberline {3}Data and Monte Carlo samples}{7}{section.3}
\contentsline {section}{\numberline {4}Impact Parameter with respect to Primary Vertex}{9}{section.4}
\contentsline {subsection}{\numberline {4.1}Factors effecting the impact parameter resolution}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Fitting of the distribution}{12}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Track IP resolution}{12}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Track IP bias}{14}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Comparison of tracks with versus without IBL hit}{15}{subsubsection.4.2.3}
\contentsline {section}{\numberline {5}Impact Parameter with respect to beamspot}{20}{section.5}
\contentsline {subsection}{\numberline {5.1}$b$-tagging recommendation for track classification}{20}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}MC reweighting}{20}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}IP resolution distribution for different categories of tracks}{20}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}MC smearing}{20}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Resolution dependence on track density}{20}{subsection.5.5}
\contentsline {subsection}{\numberline {5.6}Tail fitting}{20}{subsection.5.6}
\contentsline {section}{\numberline {6}Conclusion}{21}{section.6}
\contentsline {part}{Appendices}{25}{part*.20}
