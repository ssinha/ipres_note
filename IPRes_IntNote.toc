\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Impact Parameter of tracks}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Factors affecting the impact parameter resolution}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Deconvolution of primary vertex resolution from track impact parameter resolution}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Fitting function}{8}{subsection.2.3}
\contentsline {section}{\numberline {3}Technical details}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Ntuple generation}{11}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Event Selection}{11}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}General selection}{11}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Loose/Tight selection}{12}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Track classification used in $b$-tagging}{12}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Track classification on truth level}{13}{subsubsection.3.2.4}
\contentsline {subsection}{\numberline {3.3}MC reweighting}{14}{subsection.3.3}
\contentsline {section}{\numberline {4}Data and Monte Carlo samples}{16}{section.4}
\contentsline {section}{\numberline {5}Impact Parameter with respect to Primary Vertex}{18}{section.5}
\contentsline {subsection}{\numberline {5.1}Track IP resolution}{18}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Track IP bias}{19}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Comparison of tracks with versus without IBL hit}{21}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Variation with transverse momentum of the tracks}{22}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Variation with pseudorapidity value of the tracks}{23}{subsubsection.5.3.2}
\contentsline {section}{\numberline {6}Impact Parameter with respect to beamspot}{26}{section.6}
\contentsline {subsection}{\numberline {6.1}$d_0$ resolution of tracks}{26}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}IP resolution for different track selections}{27}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}$b$-tagging recommendation for track classification}{27}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Loose/Tight selection}{30}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Truth selection of tracks}{34}{subsubsection.6.2.3}
\contentsline {subsection}{\numberline {6.3}Parameterization of tails}{36}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Resolution dependence on track density}{39}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Resolution with time}{40}{subsection.6.5}
\contentsline {section}{\numberline {7}Conclusion}{44}{section.7}
\contentsline {part}{Appendices}{48}{part*.46}
