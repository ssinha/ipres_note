
\section{Impact Parameter with respect to Primary Vertex}
\label{sec:pv}

	\comment{
	As mentioned in section \ref{subsection:Factors effecting IPres}, the impact parameter distribution is a result of many factors. Therefore, the shape of the distribution is a result of the contribution from the tracks of various qualities. Majority of the tracks that contribute towards the core of the distribution are "prompt" tracks. These can be described well by a single Gaussian function:
	\begin{equation}
		f(d_0) = \frac{1}{\sqrt{2\pi\sigma}}\exp\Bigg[-\frac{(d_0-\mu)^2}{2\sigma^2}\Bigg]
	\end{equation}
	where, the mean and the width of the Gaussian are given by $\mu$ and $\sigma$, respectively. The mean, here determines the offset of the $d_0$ distribution with respect to the interaction point, hence called the "bias", whereas, the $\sigma$ gives the IP resolution value.
	
	If one takes into account all tracks in the distribution, the Gaussian no longer describes the fit very well. There is a significant fraction of tracks which populate the tails of the distribution. Therefore, if a Gaussian is to be fitted to the curve, the fitting must be performed within the core of the distribution. Hence, the fitting range is restricted to a range of [-1.5$\sigma$, 1.5$\sigma$] for the first fit iteration, and is adjusted for next iterations until the fitted $\sigma$ value obtained is stable within 0.5\%.
	}	

	From section \ref{subsection:Fitting}, it was established that a fit needs to be performed over the transverse as well as the longitudinal distribution of impact parameters ($d_0$ and $z_0$) in order to get the resolution values. If only the prompt tracks are to be considered, one needs to perform a Gaussian fit in the core of the distribution.
	
	Since, the values of impact parameter resolution has a huge dependence on the transverse momentum and the polar angle of the tracks, the analysis is performed over asymmetric bins in $p_T$ and $\eta$. The binning used is such that at lower track momentum, where the values of $d_0$ and $z_0$ change drastically, the bins are considerably small, and at higher $p_T$ where the values are much stable, the bins are larger.
	
	In this section, the results of the fittings performed in the core of the IP distribution is shown, i.e., an iterative Gaussian fit over the initial range of [-1.5$\sigma$, 1.5$\sigma$].

		\subsection{Track IP resolution}
		\label{subsubsection:TrackIPRes}
		
		In this section, the impact parameter resolution is shown as a function of transverse momentum of tracks for the data obtained from different years. The data from 2017 is divided into two parts for reasons explained in Section \ref{sec:samples}.
		
		In each plot, the IP resolutions are presented for the data from year: 2016 (in black), 2017- run number<337052 (in red), 2017- run number$\geq$337052 (in blue), and 2018 (in green). There is a ratio pad on the bottom of each plot which shows the ratio of the resolution values of different years with respect to the ones for 2016 data. 
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0_vs_pTas_EtaSlice1_res_SingleGauss_FRM1p5.pdf}
				\comment{old plot: d0res_SingleGauss_all.pdf}
				\label{fig:SubfigureExample1}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0res_SingleGauss_all.pdf}
				\label{fig:SubfigureExample2}
			}
			\caption{Track impact parameter resolution for tracks with |$\eta$|<0.8 after unfolding the contribution of the primary vertex
					\protect\subref{fig:SubfigureExample1} in transverse plane ($\sigma_{d_0}$), and 
					\protect\subref{fig:SubfigureExample2} along the longitudinal direction ($\sigma_{z_0}$). 
					The bottom panel shows the ratio between data of different years with respect to 2016 data.  It should be noted that the momentum axis is in logarithmic scale.}
			\label{fig:subfigexample}
		\end{figure}
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0res_SingleGauss_all_EtaSlice2.pdf}
				\label{fig:d0etaslice2}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0res_SingleGauss_all_EtaSlice2.pdf}
				\label{fig:z0etaslice2}
			}
			\caption{Track impact parameter resolution for tracks with 2.1<|$\eta$|<2.5 after unfolding the contribution of the primary vertex
				\protect\subref{fig:d0etaslice2} in transverse plane ($\sigma_{d_0}$), and 
				\protect\subref{fig:z0etaslice2} along the longitudinal direction ($\sigma_{z_0}$). 
				The bottom panels show the ratio between data of different years with respect to 2016 data.}
			\label{fig:etaslice2}
		\end{figure}
		
		Figures \ref{fig:SubfigureExample1} and \ref{fig:SubfigureExample2} show the IP resolution values ($\sigma_{d_0}$ and $\sigma_{z_0}$) for tracks with pseudorapidity ranging from -0.8 to 0.8, varying with respect to the transverse momentum of tracks. In the low $p_T$ region, the resolution is dominated by multiple scattering affects. At higher values of $p_T$, the values become comparatively flat. From the plots, it can be observed that the resolution decreases (improves) with increasing track $p_T$, hence validating Equation \ref{equation:inversepTrelation}. The plots for both $d_{0}$ and $z_{0}$ show the best resolution for 2016 data, whereas the values for other years get worse by two to five percent.
		
		
		Similar dependence on $p_T$ can be seen in Figure \ref{fig:d0etaslice2} and \ref{fig:z0etaslice2}, which correspond to the tracks with $2.1<|\eta|<2.5$. Comparing with the plots in Figure \ref{fig:subfigexample}, the $\eta$ values are higher, and hence, the resolution, on an average, becomes worse. More details on the dependence of resolution on pseudorapidity is discussed in the second part of section \ref{subsubsection:IBLhitplots}.
		
		\subsection{Track IP bias}
		
		As mentioned in section \ref{subsection:Fitting}, bias is the offset of the impact parameter distribution with respect to the interaction point. A bias on the measurement of resolution may arise due to detector distortions or remaining misalignments \cite{ATLAS-CONF-2014-047}. It affects a number of physics measurements such as lifetime measurements and vertex reconstruction. It is determined from the mean value of the fitted Gaussian function.
		
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0bias_SingleGauss_all.pdf}
				\label{fig:d0bias}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0bias_SingleGauss_all.pdf}
				\label{fig:z0bias}
			}
			\caption{Track impact parameter bias for tracks with |$\eta$|<0.8 after unfolding the contribution of the primary vertex
				\protect\subref{fig:d0bias} in transverse plane ($\mu_{d_0}$), and 
				\protect\subref{fig:z0bias} along the longitudinal direction ($\mu_{z_0}$). 
				}
			\label{fig:bias}
		\end{figure}
		
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0bias_SingleGauss_all_EtaSlice2.pdf}
				\label{fig:d0biasetaslice2}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0bias_SingleGauss_all_EtaSlice2.pdf}
				\label{fig:z0biasetaslice2}
			}
			\caption{Track impact parameter resolution for tracks with 2.1<|$\eta$|<2.5 after unfolding the contribution of the primary vertex
				\protect\subref{fig:d0biasetaslice2} in transverse plane ($\mu_{d_0}$), and 
				\protect\subref{fig:z0biasetaslice2} along the longitudinal direction ($\mu_{z_0}$). 
				}
			\label{fig:biasetaslice2}
		\end{figure}
	
		Figures \ref{fig:d0bias} and \ref{fig:z0bias} show the bias of the $d_0$ and $z_0$ distribution, respectively, for tracks with $|\eta|<0.8$. $d_0$ bias arise from weak modes of alignment:
		a rotation of IBL or radial distortions of pixel layers. For 2016 data, a bias of 2-3 $\mu$m is seen. 
		This bias was introduced by a change in the underlying geometry description of the ATLAS ID and a misconfiguration of the beam-spot constraint \cite{Aad:2724037}. An overall bias of less than $1\mu$m is seen for data collected in 2017 and 2018. Regarding the longitudinal IP bias, the values are negligible ($\sim1 \mu$m) and almost constant over the years. However, for both, $d_0$ and $z_0$, an instability is seen at low $p_T$ values. 
		
		As we move to a higher value of pseudorapidity, the $d_0$ bias values become more flat (at low $p_T$ values as well). It can be seen from Figure \ref{fig:d0biasetaslice2}. However, on an average, the longitudinal bias increases (Figure \ref{fig:z0biasetaslice2}) for higher track $\eta$. 
				
		A small bias is present in simulation as well \cite{Aad:2724037}. Consequently, it can be said that it is not introduced by the track-based alignment since it is not applied to simulation (where a perfect alignment is assumed).
		
		\subsection{Comparison of tracks with versus without IBL hit}
		\label{subsubsection:IBLhitplots}
		
		Insertable B-Layer, called the IBL, is the innermost pixel detector layer in ATLAS. With a radius of 3.3 cm, it is located extremely close to the interaction point. Since the introduction of this layer in the detector (in 2014), the tracking performance of the ATLAS Inner Detector has been significantly improved \cite{BACKHAUS201665}. Various studies comparing Run-2 to Run-1 data have already shown a significant improvement in the track impact parameter resolution by up to a factor of 2 for both transverse and longitudinal components for low $p_T$ tracks \cite{Rodina:2630663}.
		
		This section shows the comparison of the impact parameter resolution values for tracks having a hit in the IBL, with the tracks which do not show any hit in this layer. Figure \ref{fig:distribution} shows the $d_0$ distribution for tracks for 2016 data for two $p_T$ ranges. At low $p_T$ (Figure \ref{fig:1to1p25}), the distribution looks broader than the one for a higher $p_T$ value (Figure \ref{fig:10to12}). For tracks with no hits in the IBL, the central peak is much smaller compared to the tracks with IBL hits. From these plots, it can be observed that a significant fraction of track-IP measurements populate the tails of the distribution for tracks without any IBL hits. This may occur due to reconstruction issues, contamination from poorer quality tracks, or due to the presence of secondary particles from hadronic interaction with the material, or a contribution from long-lived heavy flavour hadrons.
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/NORMdata16_distribution_1to1p25.pdf}
				\label{fig:1to1p25}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/NORMdata16_distribution_10to12.pdf}
				\label{fig:10to12}
			}
			\caption{Impact parameter distribution in transverse plane ($d_0$) for tracks without any hits in the IBL, versus, with at least one IBL hit, for 
				\protect\subref{fig:1to1p25} $1<p_{T}<1.25$ GeV
				\protect\subref{fig:10to12} $10<p_{T}<12$ GeV.
				The points in red represent the tracks with no hits in the IBL, while the black points show the distribution for tracks with at least one IBL hit.
			}
			\label{fig:distribution}
		\end{figure}
		
		\subsubsection{Variation with transverse momentum of the tracks}	
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0_IBL_EtaSlice1.pdf}
				\label{fig:IBLd0_EtaSlice1}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0_IBL_EtaSlice1.pdf}
				\label{fig:IBLz0_EtaSlice1}
			}
			\caption{Track impact parameter resolution, for tracks with |$\eta$|<0.8, without any hits in the IBL, versus, with at least one IBL hit, 
				\protect\subref{fig:IBLd0_EtaSlice1} in transverse plane ($\sigma_{d_0}$), and 
				\protect\subref{fig:IBLz0_EtaSlice1}  along the longitudinal direction ($\sigma_{z_0}$)
				The bottom two panels show the ratio between data of different years with respect to 2016 data for the case with no IBL hit (middle pad), and with at least one IBL hit (bottom pad). 
				}
			\label{fig:IBL_EtaSlice1}
		\end{figure}
	
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0_IBL_EtaSlice2.pdf}
				\label{fig:IBLd0_EtaSlice2}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0_IBL_EtaSlice2.pdf}
				\label{fig:IBLz0_EtaSlice2}
			}
			\caption{Track impact parameter resolution, for tracks with 2.1<|$\eta$|<2.5, without any hits in the IBL, versus, with at least one IBL hit, 
				\protect\subref{fig:IBLd0_EtaSlice2} in transverse plane ($\sigma_{d_0}$), and 
				\protect\subref{fig:IBLz0_EtaSlice2} along the longitudinal direction ($\sigma_{z_0}$)
				The bottom two panels show the ratio between data of different years with respect to 2016 data for the case with no IBL hit (middle pad), and with at least one IBL hit (bottom pad). 
				}
			\label{fig:IBL_EtaSlice2}
		\end{figure}
				
				
		Figure \ref{fig:IBL_EtaSlice1} shows a plot for $d_0$ and $z_0$ for the tracks with at least one hit in the IBL and for tracks with no IBL hit. Evidently, the resolution is better for the case when the tracks have at least one hit in the IBL. For $d_0$, an improvement of a factor of 2 can be seen for low track $p_T$ values, while for higher $p_T$, the resolution becomes equal for both the cases (Figure \ref{fig:IBLd0_EtaSlice1}). Whereas, for the longitudinal IP resolution, one can observe from Figure \ref{fig:IBLz0_EtaSlice1}, that apart from very low $p_T$, almost a constant improvement in resolution is seen. This is because the pixel size along the $z$-direction is almost twice for the tracks with no IBL hit in comparison to the tracks with a hit in the IBL. This was the case for tracks with pseudorapidity of $|\eta|<0.8$.
		
				
		For a higher value of pseudorapidity ($2.1<|\eta|<2.5$), Figure \ref{fig:IBLd0_EtaSlice2} and \ref{fig:IBLz0_EtaSlice2} show the respective distributions for $d_0$ and $z_0$. The $d_0$ resolution gets extremely worse at very low $p_T$ when tracks do not have a hit in the IBL. The fittings for this case do not work very well. Moving further, an improvement of a factor as high as 9 can be seen for low $p_T$ values. On the other hand, $z_0$ resolution does not show any major improvement as $d_0$ does. It becomes better almost by a constant factor of 1.5 throughout the range of $p_T$ values.  
	
		\subsubsection{Variation with pseudorapidity value of the tracks}	
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/d0_IBL_Eta.pdf}
				\label{fig:d0IBL_Eta}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/z0_IBL_Eta.pdf}
				\label{fig:z0IBL_Eta}
			}
			\caption{Track impact parameter resolution versus pseudorapidity for tracks with at least one IBL hit, and, for tracks without any hits in the IBL,
				\protect\subref{fig:d0IBL_Eta} in transverse plane ($\sigma_{d_0}$)  
				\protect\subref{fig:z0IBL_Eta} along the longitudinal direction ($\sigma_{z_0}$).
				The bottom panels show the ratio between data of different years with respect to 2016 data.
				}
			\label{fig:versusEta}
		\end{figure}
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/WithIBLHit/d0_vs_Eta_bias_log_SingleGauss.pdf}
				\label{fig:d0biaswith}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/WithoutIBLHit/d0_vs_Eta_bias_log_SingleGauss.pdf}
				\label{fig:d0biaswithout}
			}
			\caption{Track impact parameter bias versus pseudorapidity in transverse plane ($\mu_{d_0}$),
				\protect\subref{fig:d0biaswith} for tracks with at least one IBL hit, and 
				\protect\subref{fig:d0biaswithout} for tracks without any hits in the IBL. 
				}
			\label{fig:d0bias_comparison}
		\end{figure}
		
		\begin{figure}[htbp]
			\centering
			\subfloat[]{
				\includegraphics[width=0.49\textwidth]{../logos/figures/IP_PV/plots/WithIBLHit/z0_vs_Eta_bias_log_SingleGauss.pdf}
				\label{fig:z0biaswith}
			}
			\subfloat[]{
				\includegraphics[width= 0.49\textwidth]{../logos/figures/IP_PV/plots/WithoutIBLHit/z0_vs_Eta_bias_log_SingleGauss.pdf}
				\label{fig:z0biaswithout}
			}
			\caption{Track impact parameter bias versus pseudorapidity along the longitudinal direction ($\mu_{z_0}$),
				\protect\subref{fig:z0biaswith} for tracks with at least one IBL hit, and 
				\protect\subref{fig:z0biaswithout} for tracks without any hits in the IBL. 
				}
			\label{fig:z0bias_comparison}
		\end{figure}
		
		In Figure \ref{fig:versusEta}, impact parameter resolution is plotted against the pseudorapidity of the tracks. The resolution is constant upto $\eta \sim 1.8$ but then degrades rapidly in the end-cap region. The degradation is due to a combination of factors. A reduction in the measured radial length for tracks longitudinally exiting the tracker, produces an increase in the relative distance to extrapolate back to the beam-line, and thus causes an increased extrapolation error. Another factor is the decrease in granularity of detectors at higher pseudorapidity values.
		
		For $z_0$, however, one sees a degradation at $\eta=0$, which improves slightly with a higher $|\eta|$ ($|\eta|\sim1$). This can be attributed to the small size of clusters at $\eta=0$. This is due to the effect of charge sharing in the estimation of position of pixel clusters. In the barrel, as the crossing angle for the tracks in the pixel layers increases, the clusters broaden, thereby distributing the signal over more than one pixel, and hence, improving the resolution in position. As one moves further to $|\eta|>1$, a subsequent deterioration is seen, accounting for the reasons similar to $d_0$. 
		
		
		Figure \ref{fig:d0bias_comparison} shows the $d_0$ bias plots for tracks with, and without IBL hits. From Figure \ref{fig:d0biaswith}, an almost constant bias is seen for $|\eta|<1$, with 2016 data showing the largest bias ($\sim 2\mu$m). On the other hand, for $z_0$, the plot in Figure \ref{fig:z0biaswith} show a positive bias for $1<\eta<2$, and a negative bias for $-2<\eta<-1$. An interesting feature of this plot is that the bias values are very similar for all years, indicating that there is no significant effect of radiation damage on the impact parameter resolution of tracks seen with respect to pseudorapidity.
		
\comment{	
	\subsection{Results} 
}