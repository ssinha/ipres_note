\section{Impact Parameter of tracks}
\label{sec:theory}

	\begin{figure}[!ht]
		\centering
		\includegraphics[width=0.5\textwidth]{../logos/figures/IP_PV/IP_pos_neg.pdf}
		\caption{Positive and negative impact parameter definition}
		\label{fig:IPar} 
	\end{figure}

	Impact parameter (IP) of a track ($d_0,\ z_0$) is defined as its point of closest approach to the Primary Vertex (PV). Transverse IP, represented by $d_0$, is the IP in the ($r,\ \phi$) plane. And the $z$-coordinate corresponding to this $d_0$ is called the longitudinal IP, represented by $z_0$. Since a $B$- hadron decays along its flight path, one can utilise this information by defining signed impact parameters. There are a number of signing conventions. The one called lifetime sign classifies the tracks crossing the jet direction in front of the primary vertex as positive, otherwise, assigns it a negative value. Figure \ref{fig:IPar} shows an illustration of the definition of signed impact parameters. 

	\begin{figure}[h]
		\centering
		\includegraphics[width=0.45\textwidth]{../logos/figures/IP_PV/d0_example_plot1.pdf}
		\caption{Example plot for transverse signed impact parameter significance of tracks in $t\bar{t}$ events associated with $b$ (solid green), $c$ (dashed blue) and light-flavour (dotted red) jets~\cite{ATL-PHYS-PUB-2015-022}}
		\label{fig:d0_sig} 
	\end{figure}

	A quantity called "impact parameter significance" is defined as the signed value of IP divided by its error. Thus, the transverse and longitudinal IP are expressed as $d_0/\sigma_{d_0}$ and $z_0/\sigma_{z_0}$, respectively, where $\sigma_{d_0}$ is the uncertainty in the transverse IP and $\sigma_{z_0}$ is the uncertainty in the longitudinal IP. Typical distributions of IP significance are presented in Figure \ref{fig:d0_sig} for $b$-, $c$- and light-flavoured jets, where it can be observed that the tracks originating from $b$-jets have a higher average decay length significance than $c$- and light-flavour jets. Since both $b$- and $c$-jets have real displaced tracks, the distinction between the different kinds of jets is visible. This is one of the major methods of distinguishing between the different kind of tracks (originating from heavy flavour or light flavour jets). Therefore, a precise measurement of IP values is a necessity for a good physics analysis.


	\subsection{Factors affecting the impact parameter resolution}
	\label{subsection:Factors effecting IPres}
	When a high-energy particle passes through the different layers of a detector, it may undergo different processes that can affect the tracking performances, which may cause a degradation in the measurement of IP resolution. There are a number of factors affecting the resolution value, such as the intrinsic single-hit resolution, level of alignment of the tracking components, multiple-scattering inside the detector material, accuracy of track reconstruction algorithms, etc. The factors which affect the resolution value the most can be described as follows: 
	
	\textbf{Finite single point resolution}: It accounts for the intrinsic resolution of the detector (and its misalignments). For simplicity, one can consider a two-point approximation and assume a basic detector of two layers, positioned at $r_1$ and $r_2$, with a single hit resolution of $\sigma_{1}$ and $\sigma_{2}$, respectively. With this approximation, one can consider two situations: ($\sigma_{2}=0, \sigma_{1}=$ finite), and ($\sigma_{1}=0, \sigma_{2}=$ finite). These situations are depicted in Figure \ref{fig:intrinsic}.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.6\textwidth]{../logos/figures/IP_PV/Intrinsic.pdf}
		\caption{Diagrammatic representation of the case when single hit resolution at layer 2 is zero, i.e., $\sigma_{2}=0$ (left), versus the situation when the same in layer 1 is zero, i.e., $\sigma_{1}=0$ (right)}
		\label{fig:intrinsic} 
	\end{figure}
	
	Hence, the contribution to $\sigma_{b}$ from the two layers can be calculated as:
	\begin{equation}
		\frac{\sigma_{b}}{\sigma_{1}}=\frac{r_{2}}{r_{2}-r_{1}};\hspace{1cm} 	\frac{\sigma_{b}}{\sigma_{2}}=\frac{r_{1}}{r_{2}-r_{1}}
	\end{equation}
	
	Therefore, 
	\begin{equation}
		\label{equation:intrinsic}	
		\sigma_{\textrm{intrinsic}}=
		\sqrt{\Bigg(\frac{r_{1}}{r_{2}-r_{1}} \sigma_{2}\Bigg)^{2} +
			\Bigg(\frac{r_{2}}{r_{2}-r_{1}} \sigma_{1}\Bigg)^{2}}
		=\sqrt{\frac{r_{1}^{2}\sigma_{2}^{2} + 	r_{2}^{2}\sigma_{1}^{2}}{(r_{2}-r_{1})^{2}}}
		\approx a
	\end{equation}
	which depends on the position of the two layers, and the space-point precision of the hit. Hence, given the radii of the different detector layers and their corresponding intrinsic resolutions stay the same, the contribution to IP resolution, as a result stays constant.
	
	To optimise the value of IP resolution, one needs a high precision measurement, i.e., a small $\sigma$ value, and a placement of the first detector as close to the point of origin of the particle as possible.
	
	\textbf{Multiple scattering}: The other factor that effects the IP resolution the most is the multiple scattering inside the detector material. When a charged particle traverses through the detector elements of the tracking system, it undergoes small deviations from its track due to Coulomb interactions with the nucleus of the detector material. It  is  roughly  Gaussian for small deflection angles, but at larger angles, it behaves like Rutherford scattering, having larger tails than that for a Gaussian distribution \cite{PhysRevD.98.030001}.
	
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.5\textwidth]{../logos/figures/IP_PV/Mult_scatt.pdf}
		\caption{Multiple scattering inside a detector material. The particle hitting the detector layer at position $r_1$ deviates from its original path inside the detector and comes out at an angle $\omega$ with respect to the incident track. This is the depiction for the case when the particle hits the detector layer perpendicularly.}
		\label{fig:m_scatt} 
	\end{figure}
	
	If one considers a charged particle passing through a detector layer at position $r_1$, with a momentum $p$, deviating by an angle $\omega$, as shown in Figure \ref{fig:m_scatt}, the resolution due to multiple scattering is given by:
	
	\begin{equation}
		\label{equation:MS}
		\sigma_{\textrm{MS}}= r_{1}\tan\omega \approx \frac{r_{1}}{p} 13.6\;\textrm{MeV}\sqrt{\frac{x_1}{X_{0}}}\;\Bigg[1+0.038\;\textrm{ln}\Bigg(\frac{x_1}{X_{0}}\Bigg)\Bigg]
	\end{equation}
	where, $x_{1}/X_0$ is the thickness of the medium traversed by the particle in units of radiation length \cite{Gupta:1279627}, $X_0$.
	
	For a more general case, when the particle is not striking perpendicularly on the detector layer (for cylindrical layers), the variables change by:
	\begin{equation}
		r_{1} = \frac{r}{\sin\theta};\;x_{1} = \frac{x}{\sin\theta}
	\end{equation}
	
	Therefore, the equation for multiple scattering (Equation \ref{equation:MS}) changes to:
	\begin{equation}
		\sigma_{\textrm{MS}}= \frac{r}{\sin\theta}\tan\omega \approx \frac{r}{p\sin\theta\sqrt{\sin\theta}} 13.6\;\textrm{MeV}\sqrt{\frac{x}{X_{0}}}\;\Bigg[1+0.038\;\textrm{ln}\Bigg(\frac{x}{\sin\theta X_{0}}\Bigg)\Bigg]
	\end{equation}
	
	From this equation, one can observe that the resolution due to multiple scattering depends on the momentum and the polar angle of the particle track, and, the position and thickness of the material of the detector. 
	Taking transverse momentum, $p_{T}=p\sin\theta$, one can approximate $\sigma_{MS}$ as:
	\begin{equation}
		\sigma_{MS} \approx \frac{b}{p_T}
	\end{equation}
	
	Since, the point measurement error and multiple scattering error are independent of each other, the total error is the quadrature sum of the two terms, i.e.:
	\begin{equation}
		\label{equation:inversepTrelation}
		\sigma_{d_0}= \sigma_{\textrm{intrinsic}} \oplus \sigma_{MS} = \sqrt{a^{2}+ \frac{b^{2}}{{p_T}^{2}}}
	\end{equation}
	which shows that the IP resolution depends on the detector geometry (the first term) and the detector material (second term), as well as the transverse momentum of the particle.
	
	
	\subsection{Deconvolution of primary vertex resolution from track impact parameter resolution}
	\label{subsection:DECONVOLUTION}
	The ability to reconstruct a single track inside the ATLAS Inner Detector (ID) is directly related to the measurements of track IP resolution. As mentioned in the introduction, the track IP resolution is a result of a convolution of the intrinsic resolution of the track, and the resolution of the event-by-event reconstructed PV. The PV resolution contributes a value of the order of $10\;\mu$m (in the transverse plane) to the IP resolution of the track. This is a significant amount for tracks with a transverse momentum of a few GeV, and hence, it is necessary that this affect be removed \cite{ATLAS-CONF-2018-006}. 
	
	Using an iterative deconvolution of the PV resolution, the track IP resolution is unfolded in both, data and simulation. Explained in detail in Ref. \cite{ATLAS-CONF-2010-070}, this unfolding procedure describes the core of the IP resolution before deconvolution, by a function:
	\begin{equation}
		\label{equation:Integral}
		R_{\textrm{meas}}(d_{0}) = \int \exp\Bigg[-\frac{1}{2} \frac{d_{0}^2}{\sigma^{2}_{d_{0},\textrm{trk}} + \sigma^{2}_{d_{0},\textrm{PV}}}\Bigg] P(\sigma_{d_{0},\textrm{PV}}) \;\textrm{d}\sigma_{d_{0},\textrm{PV}}
	\end{equation}
	where, the integrand is a Gaussian function with a width equal to the square root of the quadrature sum of the intrinsic track resolution, $\sigma_{d_{0},\textrm{trk}}$, and the PV resolution projected along the IP direction, $\sigma_{d_{0},\textrm{PV}}$. The track IP distribution before deconvolution is represented by $d_0$. This is integrated over the distribution of the PV resolution values, $P(\sigma_{d_{0},\textrm{PV}})$, corresponding to all the tracks taken into consideration. 
	
	One can obtain the deconvoluted distribution from Equation \ref{equation:Integral}, by multiplying the measured IP of each track by a correction factor:
	\begin{equation}
		\label{Equation:corr_fact}
		d_{0} \rightarrow d_{0}.\Bigg[\frac{\sigma^{2}_{d_{0},\textrm{trk}} + \sigma^{2}_{d_{0},\textrm{PV}}}{\sigma^{2}_{d_{0},\textrm{trk}}}\Bigg]^{\frac{1}{2}}
	\end{equation}  
	Substituting this in Equation \ref{equation:Integral}, the distribution of $R_{\textrm{trk}}$ is obtained as:
	\begin{equation}
		R_{\textrm{trk}}(d_{0}) = \exp \Bigg[-\frac{1}{2}\frac{d_{0}^2}{\sigma_{d_{0},\textrm{trk}}^2}\Bigg]
	\end{equation}
	
	Since the correction factor depends on the track resolution $\sigma_{d_{0},\textrm{trk}}$ , the procedure needs to be applied iteratively to obtain an approximate value, starting from the initial impact parameter, $d_0$. In the first iteration, individual values of the IP of each track are modified according to Equation \ref{Equation:corr_fact}, taking $\sigma_{d_{0},\textrm{trk}}$ and $\sigma_{d_{0},\textrm{PV}}$ as the expected resolutions from the track and primary vertex fit respectively, called
	$\sigma_{d_{0},\textrm{trk},\textrm{fit}}$ and $\sigma_{d_{0},\textrm{PV},\textrm{fit}}$.
	
	As a result, one obtains a new distribution by correcting all values of the IP. The next iteration ($i$-th iteration) is then performed, where a different correction factor is applied:
	\begin{equation}
		d_{0,\textrm{i}} \rightarrow d_{0} \Bigg[\frac{K_{\textrm{track,i-1}}^{2}\;\sigma_{d_{0},\textrm{trk},\textrm{fit}}^{2} + K_{\textrm{PV}}^{2}\;\sigma_{d_{0},\textrm{PV},\textrm{fit}}^2}
		{K_{\textrm{track,i-1}}^{2}\; \sigma_{d_{0},\textrm{trk},\textrm{fit}}^{2}}\Bigg]^{-\frac{1}{2}}
	\end{equation}   
	where, $d_0$ is the value of the original IP before the first iteration, and the errors ($\sigma_{d_{0},\textrm{trk},\textrm{fit}}$ and $\sigma_{d_{0},\textrm{PV},\textrm{fit}}$) are taken track-by-track from the nominal track and vertex fits. Additionally, factors $K_{PV}$ and $K_\textrm{track,i-1}$ are introduced. These are the scale factors for the PV, and the single track, respectively. 
	
	The scale factor $K_{PV}$ takes into account the difference in the PV resolution in data with respect to the predicted resolution by the vertex fit. It is obtained by an independent data-driven method, in which tracks forming the PV are randomly split into two sets, and two new primary vertices are obtained. These new primary vertices are fitted and their reconstructed positions are compared. The width of the "pulls\footnote{Pulls (here) are defined as the ratio of separation between the two vertices, and the error \cite{ATLAS-CONF-2010-069}.}" provides an estimate of the $K$ factors . Hence, the $K_{PV}$ values can be estimated as:
	\begin{equation}
		K_{PV} = \frac{z_{1}-z_{2}}
		{\sqrt{\sigma_{z,\textrm{1}}^{2} + \sigma_{z,\textrm{2}}^{2}}}
	\end{equation}
	where, $(z_{1}-z_{2})$ is the separation between the two vertices and $\sqrt{\sigma_{z,\textrm{1}}^{2} + \sigma_{z,\textrm{2}}^{2}}$ is the error in measurement. 
	
	The other $K$-factor, $K_\textrm{trk}$, changes with each iteration and is common for all tracks in a given $p_T$ and $\eta$ range. It is defined as:
	\begin{equation}
		K_{\textrm{track,i-1}} = \frac{\sigma_{d_{0},\textrm{trk,i-1}}}{\hat{\sigma}_{d_{0},\textrm{fit}}}
	\end{equation}
	where, $\sigma_{d_{0},\textrm{trk,i-1}}$ is the ratio of the estimated resolution derived in the previous iteration ($i$-1) of the method, and $\hat{\sigma}_{d_{0},\textrm{fit}}$ is the resolution obtained by the fit errors of tracks in the corresponding $p_T$ and $\eta$ range. To obtain the value of $\hat{\sigma}_{d_{0},\textrm{fit}}$, the IP distribution is first obtained by randomly generating values of $d_0$ distributed according to a Gaussian function with a width corresponding to the distribution of $\sigma_{d_{0},\textrm{fit}}$ of all tracks in this bin. Then the predicted resolution value $\hat{\sigma}_{d_{0},\textrm{fit}}$ is obtained by using an iterative Gaussian fit in the "core" of the distribution [$-1.5\sigma, 1.5\sigma$]. The iterations stop when $K \approx 1$ within a few percent, resulting in $\sigma_{d_{0},\textrm{trk,i-1}} \approx \sigma_{d_{0},\textrm{trk}}$.
	
	
	
	\subsection{Fitting function}
	\label{subsection:Fitting}
	The impact parameter distribution is effected by many factors (Section \ref{subsection:Factors effecting IPres}). Therefore, the shape of the distribution is a result of the contribution from the tracks of various qualities. Majority of the tracks that contribute towards the core of the distribution are "prompt" tracks. These can be described well by a single Gaussian function:
	\begin{equation}
		\label{equation:gauss}
		f(d_0) = \frac{1}{\sqrt{2\pi\sigma}}\exp\Bigg[-\frac{(d_0-\mu)^2}{2\sigma^2}\Bigg]
	\end{equation}
	where, the mean and the width of the Gaussian are given by $\mu$ and $\sigma$, respectively. The mean, here determines the offset of the $d_0$ distribution with respect to the interaction point, hence called the "bias", whereas, the $\sigma$ gives the IP resolution value.
	
	If one takes into account all tracks in the distribution, the Gaussian no longer describes the fit very well. There is a significant fraction of tracks which populate the tails of the distribution. Therefore, if a Gaussian is to be fitted to the curve, the fitting must be performed within the core of the distribution. Hence, the fitting is restricted to a range of [-1.5$\sigma$, 1.5$\sigma$] for the first fit iteration, and is adjusted for next iterations until the fitted $\sigma$ obtained is stable within 0.5\%. Figure \ref{fig:Gauss} shows such a Gaussian fit to the $d_0$ distribution. 
	\comment{
	\begin{figure}[!ht]
		\centering
		\def\svgwidth{\columnwidth}
		\includegraphics[width=0.5\textwidth]{../logos/figures/tech/TailFitting_8to9GeV.pdf}
		\caption{Example of a fit using Gaussian with exponential continuation function (PUT TWO PLOTS: GAUSSIAN AND TAIL) }
	\end{figure}
	}	
	\begin{figure}[htbp]
		\centering
		\subfloat[]{
			\includegraphics[width=0.48\textwidth]{../logos/figures/IP_PV/Gauss_250to400.pdf}
			\label{fig:Gauss}
		}
		\subfloat[]{
			\raisebox{1mm}{\includegraphics[width= 0.465\textwidth]{../logos/figures/IP_PV/New36to40_Tailfit.pdf}}
			\label{fig:Tail}
		}
		\caption{Example of a fit on $d_0$ distribution using 
			\protect\subref{fig:Gauss} a single Gaussian function (Equation \ref{equation:gauss}), and 
			\protect\subref{fig:Tail} a double Gaussian with exponential continuation function (Equation \ref{equation:tailfunction}). For a single Gaussian function, the fit is done within the core of the distribution, i.e., in a range of [-1.5$\sigma$, 1.5$\sigma$], whereas, for tail parameterization, the fitting is performed over the entire range of $d_0$ distribution. The fitted function is marked in red line, while the blue marks represent the data points.
			}
		\label{fig:Fits}
	\end{figure}

	The reasons which cause the distribution to deviate from a Gaussian include reconstruction issues, contamination from poorer quality tracks, presence of secondary particles from hadronic interaction with the material, or long-lived heavy flavour hadrons. Therefore, in order to parameterize the tails correctly, a new function must be chosen. Equation \ref{equation:tailfunction} shows such a function, described by a double Gaussian plus exponential $C^1$ continuation\footnote{A continuous function is $C^1$ if also its first derivative is continuous.}, wherein the first Gaussian corresponds to the core of the IP distribution, accommodating
	PV and track IP resolution, while the Gaussian with exponential continuation (dubbed Gauss-ExpC1), describes the IP-tail distribution \cite{ATLAS-CONF-2018-006}.	
	\begin{equation}	
	\label{equation:tailfunction}
		\large
		g(d_0)=
		\begin{cases}
			\textrm{\textit{I}}\frac{(1-f_T)}{\sqrt{2\pi}\sigma_C}\textrm{exp}\Big[-\frac{1}{2}\frac{(d_0-\mu)^2}{\sigma_C^2}\Big]+\ \textrm{\textit{I}}\frac{1}{\textrm{\textit{A}}}\frac{f_T}{\sqrt{2\pi}\sigma_T}\textrm{exp}\Big[-\frac{1}{2}\frac{(d_0-\mu)^2}{\sigma_T^2}\Big], & \textrm{for}\ |d_0-\mu|<\textrm{\textit{K}}\sigma_T
				\\[6pt]
			\textrm{\textit{I}}\frac{(1-f_T)}{\sqrt{2\pi}\sigma_C}\textrm{exp}\Big[-\frac{1}{2}\frac{(d_0-\mu)^2}{\sigma_C^2}\Big]+\ \textrm{\textit{I}}\frac{1}{\textrm{\textit{A}}}\frac{f_T}{\sqrt{2\pi}\sigma_T}\textrm{exp}\Big[\frac{\textrm{\textit{K}}^2}{2}-\textrm{\textit{K}}\frac{|d_0-\mu|}{\sigma_T}\Big], &  \textrm{for}\ |d_0-\mu|\geq \textrm{\textit{K}}\sigma_T\\
		\end{cases}	
	\end{equation}
	where, $I$ is the integral of the $d_0$ distribution, the core Gaussian is characterised by the width $\sigma_C$, the fraction of events included in the “tail” part of the function is described by $f_T$, so that $1-f_{T}=f_C$ is the complementary fraction of events contained in the core Gaussian, $\mu$ parameterises any offset in the $d_0$ distribution, and finally, the Gauss-ExpC1 tail function is characterised by the width $\sigma_T$ and the parameter $K$ describing the number of sigmas at which the Gaussian switches to the exponential decay in a continuous and $C^1$ way (these smoothness requirements ease the convergence of the fits); the normalisation factor $1/A$ is fixed by:
	\begin{equation}
		A=\textrm{erf}\;\Big(\frac{K}{\sqrt{2}}\Big)+ \frac{e^\frac{-K^2}{2}\sqrt{\frac{2}{\pi}}}{K}
	\end{equation}

	Figure \ref{fig:Tail} shows an example of such a fit on track $d_0$ distribution. The function shows a satisfactory fit to data.